ehash
=====

A library to consistently crypto-hash Erlang Terms with sha, sha256 or sha512 . The main motivation
is that commutative data structures like maps are not hashed in order as was the case with e.g. hashing
a simple sequential serialisation, but have the same hash value independant of order.


You can then create UUID5 using SHA1. Alternatively you can use return an hexadecimal binary.

A function `term_to_binary/1` also exists to encode a reproducible binary object.


Usage example
-------------

```erlang
Eshell V9.1.5  (abort with ^G)
1> application:ensure_all_started(ehash).
{ok,[ehash]}
2>  H = ehash:hash(sha256, #{ a => 1 }).
<<13,86,247,170,179,248,122,230,138,99,213,177,26,88,93,
  97,183,174,147,247,65,187,215,110,99,32,224,85,61,...>>
3>  ehash:to_hex(H).
<<"0d56f7aab3f87ae68a63d5b11a585d61b7ae93f741bbd76e6320e0553ddcca6d">>
4> ehash:uuid5(#{ a => 1 }).
<<"7a5f7f92-bc08-5b25-998e-9500cea9b41e">>
```


Build
-----

    $ rebar3 compile

To buld the documentation run the following command line:

    $ rebar3 edoc


Config
------

`ehash` is can be used via [hex.pm](https://hex.pm/packages/ehash).
