-module(ehash_SUITE).

-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).


-export([t_hash/1, t_term_to_binary/1, t_binary_to_term/1]).

all() -> [
  t_hash,
  t_term_to_binary,
  t_binary_to_term
].

init_per_suite(Config) -> Config.

init_per_testcase(_, Config) -> Config.

end_per_testcase(_, _Config) -> ok.

end_per_suite(Config) -> Config.


t_hash(_Config) ->
  H = ehash:hash(sha, doc1()),
  H = ehash:hash(sha, doc2()),
  H = ehash:hash(sha, doc3()).


t_term_to_binary(_Config) ->
  Bin = ehash:term_to_binary(doc1()),
  Bin = ehash:term_to_binary(doc2()),
  Bin = ehash:term_to_binary(doc3()).

t_binary_to_term(_Config) ->
  D = doc1(),
  D = binary_to_term(ehash:term_to_binary(D)).

doc1() ->
  #{ a => [b, c],
     b => c,
     d => #{ a => 1, b => 2 },
     e => {f, g}
   }.

doc2() ->
  #{ b => c,
     a => [b, c],
     e => {f, g},
     d => #{ b => 2, a => 1}
   }.

doc3() ->
  #{ d => #{ b => 2, a => 1 },
     a => [b, c],
     b => c,
     e => {f, g}
   }.
